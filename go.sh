#!/bin/bash
set -e
t="$(mktemp -d)"
(
h="-O2 -Wall -hidir=${t} -odir=${t} --make"
ghc ${h} -o "${t}/BoolFuckRot13" "BoolFuckRot13.hs" 1>&2
ghc ${h} -o "${t}/ReFormat" "ReFormat.hs" 1>&2
g++ -o "${t}/boof" "main.cpp" "boolfuck.cpp"
"${t}/BoolFuckRot13" |
(
cat <<"EOF"
| ###################################################### |
EOF
echo "ROT13" |
figlet -f block |
tr "_| " "##o" |
uniq |
sed 's_^\(.*\)$_| ##oo\1## |_g'
cat <<"EOF"
| ###################################################### |
 }------------------------------------------------------{ 
EOF
) > "${t}/template"
tr '-' '*' <<"EOF"
         ------------------------------------------------------------         
    ----------------------------------------------------------------------    
  --------------------------------------------------------------------------  
 --------                                                            -------- 
EOF
(
cat <<"EOF"
/--------------------------------------------------------\
EOF
(
"${t}/BoolFuckRot13" |
tr -cd "+<>[],;"
echo "(C) 2011 Claude Heiland-Allen # This is free software with NO WARRANTY: see {http://www.gnu.org/licenses/gpl.html}#" |
tr " " "_"
) |
"${t}/ReFormat" "${t}/template" |
head -n-2
cat <<"EOF"
\--------------------------------------------------------/
EOF
) |
sed 's_^\(.*\)$_********  \1  ********_g'
tr '-' '*' <<"EOF"
 --------                                                            -------- 
  --------------------------------------------------------------------------  
    ----------------------------------------------------------------------    
         ------------------------------------------------------------         
EOF
) |
sed 's_^_ _g' > "${t}/code"
"${t}/boof" "${t}/code" <<"EOF" | "${t}/boof" "${t}/code"
double-rot13, for the ultimate in privacy and discretion
EOF
cat "${t}/code" |
gzip -n9 > rot13.boof.gz
rm -r "${t}"
