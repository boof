{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances, FlexibleContexts #-}
module BoolFuck
  ( BoolFuck()
  , boolfuck
  , Concrete()
  , alloca
  , allocas
  , toggle
  , poke
  , pokes
  , byte
  , char
  , if'
  , if_
  , while
  , add
  , le
  , input
  , inputs
  , output
  , outputs
  , comment
  , move
  , assertAt
  , tell
  , Code(..)
  ) where

import Prelude hiding (Either(Left, Right))
import Control.Monad (forM_, when)
import Control.Monad.RWS (RWST, runRWST, ask, local, gets, modify, liftIO)
import Data.Bits (testBit)
import Data.Char (ord)

data Code = Toggle | Left | Right | Input | Output | Begin | End | Comment String

pretty1 :: Code -> (Bool, String)
pretty1 Toggle = (True, "+")
pretty1 Left   = (True, "<")
pretty1 Right  = (True, ">")
pretty1 Input  = (True, ",")
pretty1 Output = (True, ";")
pretty1 Begin  = (True, "[")
pretty1 End    = (True, "]")
pretty1 (Comment s) = (False, s)

type BoolFuck = RWST Int () S IO

newtype Concrete = C Int

data S = S{ ptr :: Concrete, which :: Bool, chars :: Int }

s0 :: S
s0 = S{ ptr = C 0, which = False, chars = 0 }

boolfuck :: BoolFuck () -> IO ()
boolfuck bf = runRWST bf 0 s0 >> return ()

tell :: [Code] -> BoolFuck ()
tell = mapM_ tell1

tell1 :: Code -> BoolFuck ()
tell1 c = do
  w <- gets which
  let (u, s) = pretty1 c
  when (u /= w) $ do
    liftIO . putStrLn $ if w then "\n" else ""
    modify $ \t -> t{ which = u, chars = 0 }
  if u
    then do
      n <- gets chars
      when (n == 0) $ do
        liftIO . putStr $ "\t"
        modify $ \t -> t { chars = 8 }
      liftIO . putStr $ s
      modify $ \t -> t { chars = chars t + length s }
    else do
      liftIO . putStrLn $ s
  m <- gets chars
  when (m > 70) $ do
    liftIO . putStr $ "\n"
    modify $ \t -> t { chars = 0 }

alloca :: (Concrete -> BoolFuck a) -> BoolFuck a
alloca f = do
  k <- ask
  local (+1) (f (C k))

allocas :: Int -> ([Concrete] -> BoolFuck ()) -> BoolFuck ()
allocas n f = do
  k <- ask
  local (+n) (f (map C [k .. k + n - 1]))

move :: Concrete -> BoolFuck ()
move k@(C i) = do
  C j <- gets ptr
  case compare i j of
    LT -> tell (replicate (j - i) Left)
    EQ -> return ()
    GT -> tell (replicate (i - j) Right)
  modify $ \s -> s{ ptr = k }

assertAt :: Concrete -> BoolFuck ()
assertAt k = modify $ \s -> s{ ptr = k }

comment :: String -> BoolFuck ()
comment s = let s' = filter (`notElem` "+<>,;[]") s in tell [Comment s']

input :: (Concrete -> BoolFuck b) -> BoolFuck b
input f = alloca $ \k -> do
  move k
  tell [Input]
  f k

inputs :: Int -> ([Concrete] -> BoolFuck a) -> BoolFuck a
inputs n f = inputs' n f []

inputs' :: Int -> ([Concrete] -> BoolFuck a) -> [Concrete] -> BoolFuck a
inputs' n f is
  | n <= 0 = f is
  | otherwise = input (\i -> inputs' (n - 1) f (is ++ [i]))

output :: POKE a Concrete => a -> (Concrete -> BoolFuck b) -> BoolFuck b
output b f = alloca $ \k -> poke b k >> tell [Output] >> f k

outputs :: [Concrete] -> (Concrete -> BoolFuck a) -> BoolFuck a
outputs x f = alloca $ \k -> do
  poke False k
  outputs' f x k

outputs' :: (Concrete -> BoolFuck a) -> [Concrete] -> Concrete -> BoolFuck a 
outputs' f [] k = f k
outputs' f (x:xs) _ = output x $ outputs' f xs

byte :: Int -> [Concrete] -> BoolFuck ()
byte n bs = forM_ (bs `zip` [0..]) $ \(o, b) -> poke (n `testBit` b) o

char :: Char -> [Concrete] -> BoolFuck ()
char c bs = byte (ord c) bs

while :: Concrete -> BoolFuck a -> BoolFuck a
while k m = do
  move k
  tell [Begin]
  r <- m
  move k
  tell [End]
  return r

toggle :: Concrete -> BoolFuck ()
toggle k = do
  move k
  tell [Toggle]

class IF a b where
  if' :: a -> BoolFuck b -> BoolFuck b -> BoolFuck b
  if_ :: a -> a -> BoolFuck b -> BoolFuck b -> BoolFuck b

instance IF Bool a where
  if' True  a _ = a
  if' False _ b = b
  if_ _ True  a _ = a
  if_ _ False _ b = b

instance IF Concrete () where
  if' test then' else' = alloca $ \tmp -> if_ tmp test then' else'
  if_ tmp test then' else' = do
    poke True tmp
    while tmp $ do
      while test $ do
        then'
        poke False tmp
        poke False test
      while tmp $ do
        else'
        poke False tmp
        poke True test
    move test
    tell [Toggle]

class LE a b c where
  le :: [a] -> [b] -> c -> BoolFuck ()

instance (IF a (), IF b (), POKE Bool c) => LE a b c where
  le a b o = le' (reverse a) (reverse b)
    where
      le' [] [] = poke True o
      le' (x:xs) (y:ys) = do
        if' x
          (if' y (le' xs ys) (poke False o))
          (if' y (poke True o) (le' xs ys))
      le' _ _ = error "le'"

class PEEK a b where
  peek :: a -> BoolFuck b

instance PEEK Bool Bool where
  peek = return

class POKE a b where
  poke :: a -> b -> BoolFuck ()

pokes :: POKE a b => [a] -> [b] -> BoolFuck ()
pokes a b = sequence_ (zipWith poke a b)

instance POKE Bool Concrete where
  poke b k = move k >> tell [Begin, Toggle, End] >> when b (tell [Toggle])

instance POKE Concrete Concrete where
  poke j k = if' j (poke True k) (poke False k)

class ADD a b c where
  add :: [a] -> [b] -> [c] -> BoolFuck ()

instance (IF a (), IF b (), POKE Bool c) => ADD a b c where
  add l m n = add' False l m n
    where
      add' _ [] [] [] = return ()
      add' c (x:xs) (y:ys) (z:zs) =
        (if' c
          (if' x
            (if' y
              (poke True  z >> add' True  xs ys zs)
              (poke False z >> add' True xs ys zs))
            (if' y
              (poke False z >> add' True xs ys zs)
              (poke True  z >> add' False  xs ys zs)))
          (if' x
            (if' y
              (poke False z >> add' True  xs ys zs)
              (poke True  z >> add' False xs ys zs))
            (if' y
              (poke True  z >> add' False xs ys zs)
              (poke False z >> add' False xs ys zs))))
      add' _ _ _ _ = error "add'"
