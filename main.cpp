/* boolfuck.cpp -- an implementation of the Boolfuck programming
 * language, invented by Sam Hughes (not that I'm the first).  This
 * implementation is by Sam Hughes.  Not designed for hyperefficiency
 * or anything :) -- shughes@pgsit.org */
 
/* For the rest of this document, I recommend considering the word "fuck" to be
 * non-profane.  If you find use of the word to be offensive, I recommend that
 * you see a psychiatrist. */
 
/* This document is public domain. */

#include "boolfuck.h"

#include <iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;

#include <string>
using std::string;

#include <fstream>
using std::ifstream;


int main(int argc, char ** argv)
{
	if (argc != 2) {
		cerr << "Use one argument.  Not wimping out, are you?\n";
		return 1;
	}

	ifstream fin(argv[1]);

	if (! fin) {
		cerr << "Error opening file.  Did you even make a file?\n";
		return 1;
	}

        boolfucker fucker(fin);
	fucker.fuck(cin, cout);

	/* These variable names are actually as descriptive as possible. */

	return 0;
}
