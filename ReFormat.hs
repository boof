module Main (main) where
import System.Environment (getArgs)

main :: IO ()
main = do
  template <- readFile . head =<< getArgs
  interact $ reformat (cycle $ template ++ map invert template)

invert :: Char -> Char
invert '#' = 'o'
invert 'o' = '#'
invert  c  =  c

reformat :: String -> String -> String
reformat _ [] = []
reformat [] _ = []
reformat ('#':ts) (c:cs) =  c : reformat ts cs
reformat ('o':ts) (  cs) = ' ': reformat ts cs
reformat ( t :ts) (  cs) =  t : reformat ts cs
