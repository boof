module Main (main) where

import Prelude hiding (Right)
import Control.Monad (when)
import Data.Bits (testBit)
import Data.Char (ord)
import Data.List (sort)

import BoolFuck

rot13 :: [([Bool], [Bool], Int)]
rot13 = sort . map (\c -> (b c, b (r c), c)) $ [0 .. 255]
  where
    b c = [ c `testBit` n | n <- [0 .. 7] ]
    r c
      | ord 'A' <= c && c <= ord 'M' || ord 'a' <= c && c <= ord 'm' = c + 13
      | ord 'N' <= c && c <= ord 'Z' || ord 'n' <= c && c <= ord 'z' = c - 13
      | otherwise = c

remap :: [(Concrete, Concrete, Concrete)] -> [Concrete] -> [([Bool], [Bool], Int)] -> BoolFuck ()
remap [] vs [([], ns, n)] = do
  comment $ "putByte(" ++ show n ++ ")"
  putb ns vs
remap ((i, t, o):us) vs xs = do
  comment $ "if (getBit())"
  move i
  tell [Input]
  if_ t i
    (do
      comment "{"
      remap us (o:vs) (next True xs)
      comment "}"
    )
    (do
      comment "else"
      comment "{"
      remap us (o:vs) (next False xs)
      comment "}"
    )
remap _ _ _ = error "remap"

next :: Bool -> [([Bool], [Bool], Int)] -> [([Bool], [Bool], Int)]
next b xs = [ (ys, zs, n) | (y:ys, zs, n) <- xs, y == b ]

putb :: [Bool] -> [Concrete] -> BoolFuck ()
putb [] [] = return ()
putb (n:ns) (o:vs) = do
  move o
  tell [Begin, Toggle, End]
  when n $ tell [Toggle]
  tell [Output]
  putb ns vs
putb _ _ = error "putb"

rot13fsm :: BoolFuck ()
rot13fsm = alloca $ \ok -> allocas (8*3) $ \vs -> do
  let (itos, os) = unzip . map (\(i, t, o) -> ((i, t, o), o)) . chunk3 $ vs
  comment "ok := true"
  move ok
  tell [Toggle]
  comment "while (ok)"
  tell [Begin]
  comment "{"
  remap itos [] rot13
  test ok os
  comment "}"
  move ok
  tell [End]
  comment "(EOF)"

test :: Concrete -> [Concrete] -> BoolFuck ()
test ok [] = do
  move ok
  tell [Toggle]
test ok (o:os) = do
  toggle o
  while o (test ok os >> toggle o)

chunk3 :: [a] -> [(a,a,a)]
chunk3 (x:y:z:ws) = (x,y,z) : chunk3 ws
chunk3 _ = []

main :: IO ()
main = boolfuck rot13fsm
