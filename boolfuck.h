#ifndef boolfuck_h_
#define boolfuck_h_

/* A boolpit is an infinite array of bools (growable at both ends) */
/* boolfuck.h - by Sam Hughes - shughes@pgsit.org */
/* This document is public domain. */

#include <string>
#include <iostream>
#include <vector>

class boolpit {

	unsigned char * beg_;
	unsigned char * mid_;
	unsigned char * end_;

 public:

	boolpit();
	boolpit(const boolpit & copyee);
	~boolpit();

	boolpit & operator=(const boolpit & assignee);

	void set(size_t offset, int i);
	int get(size_t offset);
	void flip(size_t offset);

 private:
	void copy(const boolpit & copyee);
	void grow_and_accommodate(unsigned char * & p);


};

class boolfucker {
	struct size_triad {
		size_t lpos, rjump, bjump;
		size_triad(size_t a, size_t b, size_t c)
			: lpos(a), rjump(b), bjump(c) {}
	};

	std::string prog_;
	std::vector<size_triad> brace_pos_;

 public:
	boolfucker(std::istream & istr);
	void fuck(std::istream & in, std::ostream & out) const;



};


#endif
